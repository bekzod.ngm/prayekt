from django.shortcuts import render, redirect
from .models import Post, Category, Comment, FeedBack
from django.db.models import Q
from .forms import RegisterForm
from django.contrib.auth import logout
from django.http import Http404, HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.models import User


def index(request):
    news = Post.objects.order_by('-date')[:3]
    popular = Post.objects.filter(views__gt=10)[:3]
    return render(request, 'blog/index.html', {'news': news, 'popular': popular})


def news_list(request):
    news = Post.objects.order_by('-date')
    return render(request, 'blog/news.html', {'news': news})


def search_result(request):
    query = request.GET.get('search')
    search_obj = Post.objects.filter(
        Q(title__icontains=query) | Q(summary__icontains=query)
    )
    return render(request, 'blog/search_result.html', {'query': query, 'search_obj': search_obj})


def post_detail(request, slug):
    post = Post.objects.get(slug__iexact=slug)
    post.views += 1
    post.save()
    return render(request, 'blog/post_detail.html', context={'post': post})


def category_detail(request, slug):
    category = Category.objects.get(slug__iexact=slug)
    posts = Post.objects.order_by('-date')
    return render(request, 'blog/category_detail.html', context={'category': category, 'posts': posts})


def category(request):
    categories = Category.objects.all()
    return render(request, 'blog/category_page.html', {'categories': categories})


def register(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = RegisterForm()
    return render(request, 'blog/register.html', {'form': form})


def leave_comment(request, slug):
    try:
        post = Post.objects.get(slug__iexact=slug)
    except:
        raise Http404("Article not found")
    if request.user.is_authenticated:
        user = request.user.first_name
        post.comment_set.create(author_name=user,
                                text=request.POST.get('text'))
    else:
        post.comment_set.create(
            author_name=request.POST.get('name'), text=request.POST.get('text'))
    return HttpResponseRedirect(reverse('post_detail_url', args=(post.slug,)))


def feedback(request):
    if request.method == "POST":
        if request.user.is_authenticated:
            first_name = request.user.first_name
            last_name = request.user.last_name
            email_user = request.user.email
            FeedBack.objects.create(name=first_name, last_name=last_name, email=email_user,
                                    phone=request.POST.get('phone'),
                                    message=request.POST.get('message'))
        else:
            FeedBack.objects.create(name=request.POST.get('name'),
                                    last_name=request.POST.get('last_name'),
                                    email=request.POST.get('email'),
                                    phone=request.POST.get('phone'),
                                    message=request.POST.get('message'))

    return render(request, 'blog/feedback.html')


def popular_posts(request):
    popular = Post.objects.filter(views__gt=10)[:3]
    return render(request, 'blog/popular_list.html', {'popular': popular})
