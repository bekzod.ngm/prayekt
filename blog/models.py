from django.db import models
from django.utils import timezone
from django.shortcuts import reverse


class Category(models.Model):
    title = models.CharField('Header', max_length=255)
    slug = models.SlugField('Link', max_length=255, unique=True)
    image = models.ImageField('Image', blank=True)

    def get_absolute_url(self):
        return reverse('category_detail_url', kwargs={'slug': self.slug})

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.title


class Post(models.Model):
    title = models.CharField('Header', max_length=255)
    slug = models.SlugField('Link', max_length=255, unique=True)
    summary = models.TextField('Short description')
    content = models.TextField('Description')
    date = models.DateTimeField('Date', default=timezone.now)
    image = models.ImageField('Image', blank=True)
    categories = models.ForeignKey(
        Category, on_delete=models.CASCADE, null=True)
    views = models.IntegerField('Views', default=0)

    def get_absolute_url(self):
        return reverse('post_detail_url', kwargs={'slug': self.slug})

    class Meta:
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'

    def __str__(self):
        return self.title


class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    author_name = models.CharField('Author name', max_length=255)
    text = models.TextField('Commentary text')
    date = models.DateTimeField('Comment date', default=timezone.now)

    class Meta:
        verbose_name = "Comment"
        verbose_name_plural = "Comments"

    def __str__(self):
        return self.author_name


class FeedBack(models.Model):
    name = models.CharField('Name', max_length=255)
    last_name = models.CharField('Last name', max_length=255)
    email = models.EmailField('Email', max_length=255)
    phone = models.CharField('Phone', max_length=255)
    message = models.TextField('Text', max_length=255)
    date = models.DateTimeField('Date', default=timezone.now)

    class Meta:
        verbose_name = 'Message'
        verbose_name_plural = 'Messages'

    def __str__(self):
        return self.name


class slider(models.Model):
    slug = models.SlugField('link', max_length=255)
    image = models.ImageField('Image')
